import System.Environment

import Language.Python.Decompile.CallPython
import Language.Python.Decompile.Show.Code

-- | The program takes the path to a pyc file as an argument,
-- disassembles it, and prints the result to standard out.
main :: IO ()
main = do
  args <- getArgs
  let path = atDefault 0 args id $ error "py37dis: No pyc file specified."
      py   = atDefault 1 args Just Nothing
  getCodeFromPython py path >>= putStrLn . showCode 0

atDefault :: Int -> [a] -> (a -> b) -> b -> b
atDefault _ []       _ b = b
atDefault 0 (a : _ ) f _ = f a
atDefault n (_ : as) f b = atDefault (n - 1) as f b
