module Language.Python.Decompile.Show.Code where

import Language.Python.Decompile.Code
import Language.Python.Decompile.Show

-- | Shows a code structure at the given indentation.
showCode :: Int -> Code -> String
showCode n Code {..}
   = indent n "arguments: " ++ show argCount ++ "\n"
  ++ indent n "  keyword: " ++ show kwCount ++ "\n"
  ++ indent n "    names:\n"
  ++ concatMap showNumName (zip [0..] names)
  ++ indent n "     vars:\n"
  ++ concatMap showNumName (zip [0..] vars)
  ++ indent n "cell vars:\n"
  ++ concatMap showNumName (zip [0..] cell)
  ++ indent n "free vars:\n"
  ++ concatMap showNumName (zip [length cell..] free)
  ++ indent n "     code:\n"
  ++ concatMap (showInstruction n) insts
  ++ indent n "constants:\n"
  ++ interMap "\n" (showValue n) consts
  where
    showNumName :: (Int, Name) -> String
    showNumName (i, s) = indent n $ padLeft 4 (show i) ++ " " ++ s ++ "\n"

-- | Shows an instruction at the given indentation.
showInstruction :: Int -> Instruction -> String
showInstruction n Instruction {..} = indent n
  $ row' ++ " " ++ opcode' ++ " " ++ argument' ++ "\n"
  where row'      = padLeft  3  $ show row
        opcode'   = padRight 30 $ show opcode
        argument' = padLeft  3  $ show argument

-- | Shows a value at the given indentation.
showValue :: Int -> Value -> String
showValue n = \case
  VCode c   -> showCode (n + 1) c
  VNone     -> indent n "None"
  VBool b   -> indent n $ show b
  VInt i    -> indent n $ show i
  VStr s    -> indent n s
  VTuple vs -> indent n $ properTuple (showValue 0) vs
