module Language.Python.Decompile.Show.AST where

import Language.Python.Decompile.AST
import Language.Python.Decompile.Code
import Language.Python.Decompile.Show

import Language.Python.Decompile.Build.AST

showAST :: Int -> AST -> String
showAST i = interMap "\n" (showStatement i)

-- | Shows a statement.
showStatement :: Int -> Statement -> String
showStatement i = \case
  Pass -> indent i "pass"
  Expr e -> indent i $ showExpr e
  Return None -> indent i "return"
  Return e -> indent i "return " ++ showExpr e
  Raise e -> indent i "raise " ++ showExpr e
  Break -> indent i "break"
  Del e -> indent i "del " ++ showExpr e
  Import n [] -> indent i "import " ++ n
  Import n ns -> indent i "from " ++ n ++ " import " ++ interMap ", " f ns
    where f (n1, n2) = n1 ++ if n1 == n2 then "" else " as " ++ n2
  Assign _ e f -> indent i $ showExpr e ++ " = " ++ showExpr f
  -- TODO: turn into an error
  Append _ e -> indent i "APPEND " ++ showExpr e
  Def e Code {..} a -> indent i "def " ++ showExpr e ++ inside Tuple id params
    ++ ":\n" ++ showAST (i + 1) a
    where params = take argCount vars
  Decorator e s -> indent i "@" ++ showExpr e ++ "\n" ++ showStatement i s
  Class e Code {..} is a -> indent i "class " ++ showExpr (Call e is)
    ++ ":\n" ++ showAST (i + 1) a
  If e a -> indent i "if " ++ showExpr e ++ ":\n" ++ showAST (i + 1) a
  Else a -> indent i "else:\n" ++ showAST (i + 1) a
  For ns e a -> indent i "for " ++ maybeTuple id ns ++ " in " ++ showExpr e
    ++ ":\n" ++ showAST (i + 1) a
  Try a -> indent i "try:\n" ++ showAST (i + 1) a
  Except Nothing a -> indent i "except:\n" ++ showAST (i + 1) a
  Except (Just n) a -> indent i "except " ++ n ++ ":\n" ++ showAST (i + 1) a
  Finally a -> indent i "finally:\n" ++ showAST (i + 1) a
  With e Nothing a -> indent i "with " ++ showExpr e
    ++ ":\n" ++ showAST (i + 1) a
  With e (Just n) a -> indent i "with " ++ showExpr e ++ " as " ++ n
    ++ ":\n" ++ showAST (i + 1) a

-- | Shows an expression.
showExpr :: Expr -> String
showExpr = \case
  Name n -> n
  None -> "None"
  Bool b -> show b
  Int i -> show i
  Float d -> show d
  Str s -> s
  Bytes b -> b
  Collection Tuple is -> properTuple (showItem False "") is
  Collection c is -> inside c (showItem False ": ") is
  Call e is -> showExpr e ++ inside Tuple (showItem True " = ") is
  Not e -> "not " ++ showExpr e
  Attr e n -> showExpr e ++ "." ++ n
  -- TODO: replace debug output with an error
  Function c -> --error "showExpr: Function should not be showed."
    "<code:\n" ++ showAST 0 (buildAST c) ++ "\n>"
  Lambda Code {..} e -> "lambda " ++ interMap ", " id (take argCount vars)
    ++ ": " ++ showExpr e
  Comprehension c e ns f -> inside c id . pure
    $ showExpr e ++ " for " ++ maybeTuple id ns ++ " in " ++ showExpr f
  BuildClass -> error "showExpr: BuildClass should not be showed."
  Binary s e f -> showExpr e ++ " " ++ s ++ " " ++ showExpr f
  Yield e -> "yield " ++ showExpr e
  YieldFrom e -> "yield from " ++ showExpr e
  Index e i -> showExpr e ++ inside List showExpr [i]

-- | Shows an item, given a separator string to use for mappings.
-- First argument indicated if a key should be shown as a name.
showItem :: Bool -> String -> Item -> String
showItem b s = \case
  Item e -> showExpr e
  ItemUnpack n -> "*" ++ n
  Mapping e e' -> k ++ s ++ showExpr e'
    where k = if b then showAsName e else showExpr e
  MappingUnpack n -> "**" ++ n

-- | Shows an expression as if it was a name.
showAsName :: Expr -> String
showAsName (Name n) = n
showAsName (Str  s) = init $ tail s
showAsName e        = error
  $ "showAsName: Can not show expression as name " ++ show e

-- | Encloses a list of elements in characters based on the collection type.
inside :: Collection -> (a -> String) -> [a] -> String
inside c f as = l c ++ interMap ", " f as ++ r c
  where l Tuple = "("
        l List  = "["
        l Gen   = ""
        l _     = "{"
        r Tuple = ")"
        r List  = "]"
        r Gen   = ""
        r _     = "}"

-- | Encloses the elements in a tuple unless it's only one element.
maybeTuple :: (a -> String) -> [a] -> String
maybeTuple f [a] = f a
maybeTuple f as  = inside Tuple f as
