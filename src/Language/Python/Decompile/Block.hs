module Language.Python.Decompile.Block where

import Language.Python.Decompile.Code

data Block
  = BInst Instruction
  | BFor     Int [Int]       [Block]
  | BTry     Int             [Block]
  | BExcept  Int (Maybe Int) [Block]
  | BFinally Int             [Block]
  | BWith    Int (Maybe Int) [Block]
