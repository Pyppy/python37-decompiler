module Language.Python.Decompile.Code where

import Language.Python.Decompile.OpCode

-- | Type synonym used for python names.
type Name = String

-- | Represents python types.
data Value
  = VCode Code
  | VNone
  | VBool Bool
  | VInt Int
  | VFloat Double
  | VStr String
  | VBytes String
  | VTuple [Value]
  deriving (Show, Eq)

-- | A python bytecode instruction.
data Instruction = Instruction
  { -- | The instruction row, incremented for each byte.
    row :: Int
    -- | The opcode.
  , opcode :: OpCode
    -- | The instruction argument.
  , argument :: Int
  } deriving (Show, Eq)

-- | A python bytecode structure.
data Code = Code
  { -- | Number of arguments the code accepts.
    argCount :: Int
    -- | How many arguments are keyword arguments.
  , kwCount :: Int
    -- | The list of names referenced in the code.
  , names :: [Name]
    -- | The list of variables referenced in the code.
  , vars :: [Name]
    -- | The list of cell variables referenced in the code.
  , cell :: [Name]
    -- | The list of free variables referenced in the code.
  , free :: [Name]
    -- | The list of constant values referenced in the code.
  , consts :: [Value]
    -- | The bytecode instruction list.
  , insts :: [Instruction]
  } deriving (Show, Eq)
