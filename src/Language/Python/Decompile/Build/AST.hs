module Language.Python.Decompile.Build.AST where

import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.State
import Data.Bits

import Language.Python.Decompile.AST
import Language.Python.Decompile.Block
import Language.Python.Decompile.Build.Block
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

buildModule :: Code -> AST
buildModule = insertPass . removeLastReturn . buildAST

-- | Builds an AST from a Code structure.
buildAST :: Code -> AST
buildAST c @ Code {..}
  -- Get the result from the identity monad.
  = runIdentity
  -- Give access to the Code structure.
  . flip runReaderT c
  -- Start with an empty stack.
  . flip evalStateT []
  -- Fix issues remaining after building
  . fmap fixAST
  -- Builds the AST from the blocks built from the instructions.
  . buildStatements . buildBlocks $ insts

-- | The monad used when building the AST.
type Build = StateT [Expr] (ReaderT Code Identity)

-- | Returns the nth name.
getName :: Int -> Build Name
getName n = asks $ \ Code {..} -> if n < length names
  then names !! n
  else error "getName: Invalid name index."

-- | Returns the nth variable name.
getVar :: Int -> Build Name
getVar n = asks $ \ Code {..} -> if n < length vars
  then vars !! n
  else error "getVar: Invalid variable name index."

-- | Returns the nth cell or free variable.
getCellFree :: Int -> Build Name
getCellFree n = asks $ \ Code {..} -> let cf = cell ++ free
  in if n < length cf
  then cf !! n else error "getConst: Invalid constant index."

-- | Returns the nth constant.
getConst :: Int -> Build Value
getConst n = asks $ \ Code {..} -> if n < length consts
  then consts !! n
  else error "getConst: Invalid constant index."

-- | Pushes an expression on top of the stack.
push :: Expr -> Build ()
push v = modify (v :)

-- | Pops an expression from the top of the stack.
pop :: Build Expr
pop = get >>= \case
  (v : vs) -> put vs >> return v
  []       -> error "pop: Empty stack."

buildStatements :: [Block] -> Build AST
buildStatements [] = return []
buildStatements (BInst i : bs)
  | opcode i == IMPORT_NAME = pop >> pop >> do
      name <- getName $ argument i
      (list, bs') <- buildImportFromList bs
      (Import name list :) <$> buildStatements (drop 1 bs')
  | opcode i == UNPACK_SEQUENCE = do
      e <- pop
      let (bs0, bs1) = splitAt (argument i) bs
      vars <- mapM (\ (BInst i') -> Name <$> getVar (argument i')) bs0
      mapM_ push $ reverse vars
      (Assign False (Collection Tuple $ map Item vars) e :)
        <$> buildStatements bs1
buildStatements (b : bs) = (:) <$> buildStatement b <*> buildStatements bs

buildImportFromList :: [Block] -> Build ([(Name, Name)], [Block])
buildImportFromList (BInst i1 : BInst i2 : bs)
  | opcode i1 == IMPORT_FROM = do
      pair <- (,) <$> getName (argument i1) <*> getName (argument i2)
      (list, bs') <- buildImportFromList bs
      return (pair : list, bs')
  | otherwise = return ([], BInst i1 : BInst i2 : bs)
buildImportFromList bs = return ([], bs)

buildStatement :: Block -> Build Statement
buildStatement (BInst i) = buildStatement' i
--buildStatement (BIf _ b bs)
--  = If <$> (if b then Not <$> pop else pop) <*> buildStatements bs
--buildStatement (BElse _ bs) = Else <$> buildStatements bs
buildStatement (BFor _ ns bs)
  = For <$> mapM getVar ns <*> pop <*> buildStatements bs
buildStatement (BTry _ bs) = Try <$> buildStatements bs
buildStatement (BExcept _ Nothing bs) = Except Nothing <$> buildStatements bs
buildStatement (BExcept _ (Just n) bs)
  = Except <$> (Just <$> getName n) <*> buildStatements bs
buildStatement (BFinally _ bs) = Finally <$> buildStatements bs
buildStatement (BWith _ Nothing bs)
  = With <$> pop <*> return Nothing <*> buildStatements bs
buildStatement (BWith _ (Just n) bs)
  = With <$> pop <*> (Just <$> getVar n) <*> buildStatements bs

buildStatement' :: Instruction -> Build Statement
buildStatement' Instruction {..} = case opcode of
  -- Addition.
  BINARY_ADD -> pass $ flip (Binary "+") <$> pop <*> pop >>= push
  -- Multiplication.
  BINARY_MULTIPLY -> pass $ flip (Binary "*") <$> pop <*> pop >>= push
  -- Indexing.
  BINARY_SUBSCR -> pass $ flip Index <$> pop <*> pop >>= push
  -- Subtraction.
  BINARY_SUBTRACT -> pass $ flip (Binary "-") <$> pop <*> pop >>= push
  -- Division.
  BINARY_TRUE_DIVIDE -> pass $flip (Binary "/") <$> pop <*> pop >>= push
  -- Break.
  BREAK_LOOP -> return Break
  -- List creation.
  BUILD_LIST -> pass $ replicateM argument pop
    >>= push . Collection List . map Item . reverse
  -- Map creation.
  BUILD_MAP -> pass $ replicateM argument (flip Mapping <$> pop <*> pop)
    >>= push . Collection Dict . reverse
  -- Map merging.
  BUILD_MAP_UNPACK_WITH_CALL -> pass $ replicateM argument pop
    >>= push . foldl mergeCollections (Collection Dict []) . reverse
  -- Map creation with constant keys.
  BUILD_CONST_KEY_MAP -> pass $ pop >>= \case
    Collection Tuple ks
      -> mapM (\ (Item e) -> Mapping e <$> pop) (reverse ks)
      >>= push . Collection Dict . reverse
    t -> error $ "BUILD_CONST_KEY_MAP: Found non-tuple tuple " ++ show t
  -- Set creation.
  BUILD_SET -> pass $ replicateM argument pop
    >>= push . Collection Set . map Item . reverse
  -- Tuple creation.
  BUILD_TUPLE -> pass $ replicateM argument pop
    >>= push . Collection Tuple . map Item . reverse
  -- Tuple merging.
  BUILD_TUPLE_UNPACK_WITH_CALL -> pass $ replicateM argument pop
    >>= push . foldl mergeCollections (Collection Tuple []) . reverse
  -- Function call.
  CALL_FUNCTION -> replicateM argument pop >>= \ args ->
    pass $ Call <$> pop <*> return (map Item $ reverse args)  >>= push
  -- Function call with kw arguments.
  CALL_FUNCTION_KW -> pass $ pop >>= \case
    Collection Tuple ks -> replicateM argument pop >>= \ args
      -> let
      kws = zipWith (\ (Item e) e' -> Mapping e e') (reverse ks) args
      pos = map Item $ drop (length ks) args
      in Call <$> pop <*> return (reverse $ kws ++ pos) >>= push
    t -> error $ "CALL_FUNCTION_KW: Found non-tuple tuple " ++ show t
  -- Function call with arguments contained in a tuple and possibly a map.
  CALL_FUNCTION_EX -> do
    kws <- if argument .&. 0x1 /= 0
      then pop >>= \case
      Name n          -> return [MappingUnpack n]
      Collection _ is -> return is
      m -> error $ "CALL_FUNCTION_EX: Found invalid kw expression " ++ show m
      else return []
    pos <- pop >>= \case
      Name n          -> return [ItemUnpack n]
      Collection _ is -> return is
      t -> error $ "CALL_FUNCTION_EX: Found invalid argument " ++ show t
    pass $ Call <$> pop <*> return (pos ++ kws) >>= push
  -- A binary comparison operation.
  COMPARE_OP -> pass
    $ flip (Binary $ toCompareOp argument) <$> pop <*> pop >>= push
  -- Indexed deletion.
  DELETE_SUBSCR -> Del <$> (flip Index <$> pop <*> pop)
  -- Pushes a second copy of the expression on the stack.
  DUP_TOP -> pass $ pop >>= \ e -> push e >> push e
  -- List appending used in list comparisons. The list remains on the stack.
  LIST_APPEND -> Append List <$> pop
  -- Attribute access.
  LOAD_ATTR -> pass $ Attr <$> pop <*> getName argument >>= push
  -- Pushes the builtin build class function.
  LOAD_BUILD_CLASS -> pass $ push BuildClass
  -- Constant loading.
  LOAD_CONST -> pass $ getConst argument >>= push . valueToExpr
  -- Variable loading.
  LOAD_FAST -> pass $ getVar argument >>= push . Name
  -- Free variable loading.
  LOAD_DEREF -> pass $ getCellFree argument >>= push . Name
  -- Method loading. We do not push the object itself.
  LOAD_METHOD -> pass $ Attr <$> pop <*> getName argument >>= push
  -- Name loading.
  LOAD_NAME -> pass $ getName argument >>= push . Name
  -- Function loading.
  MAKE_FUNCTION -> pass $ pop >> pop >>= \case
    Function c -> do
      when (argument .&. 0x1 /= 0) $ void pop -- TODO: default values
      when (argument .&. 0x2 /= 0) $ void pop -- TODO: default kw values
      when (argument .&. 0x4 /= 0) $ void pop -- TODO: type annotations
      when (argument .&. 0x8 /= 0) $ void pop
      push $ Function c
    c -> error $ "MAKE_FUNCTION: Found non-code code " ++ show c
  -- Pop the top stack object. We need to add it to the AST.
  POP_TOP -> Expr <$> pop
  -- Raise an exception.
  RAISE_VARARGS -> do
    unless (argument == 1)
      . error $ "RAISE_VARARGS: Unsupported argument " ++ show argument
    Raise <$> pop
  -- Return statement.
  RETURN_VALUE -> Return <$> pop
  -- Set appending used in set comparisons. The set remains on the stack.
  SET_ADD -> Append Set <$> pop
  -- Attribute assignment.
  STORE_ATTR -> Assign False <$> (Attr <$> pop <*> getName argument) <*> pop
  -- Free variable assignment.
  STORE_DEREF -> Assign False <$> (Name <$> getCellFree argument) <*> pop
  -- Variable assignment.
  STORE_FAST -> Assign False <$> (Name <$> getVar argument) <*> pop
  -- Global variable assignment.
  STORE_GLOBAL -> Assign True <$> (Name <$> getName argument) <*> pop
  -- Name assignment.
  STORE_NAME -> Assign False <$> (Name <$> getName argument) <*> pop
  -- Index assignment
  STORE_SUBSCR -> Assign False <$> (flip Index <$> pop <*> pop) <*> pop
  -- Yield a value. -- TODO: Double check if it's correct.
  YIELD_VALUE -> pass $ Yield <$> pop >>= push
  -- Yield a value from a generator. -- TODO: Investigate the need to pop.
  YIELD_FROM -> pass $ pop >> YieldFrom <$> pop >>= push
  -- TODO: Debugging code
  POP_JUMP_IF_FALSE -> debugOpCodeWith opcode argument . show <$> pop
  POP_JUMP_IF_TRUE -> debugOpCodeWith opcode argument . show <$> pop
  JUMP_ABSOLUTE -> return $ debugOpCodeWith opcode argument ""
  JUMP_FORWARD -> return $ debugOpCodeWith opcode argument ""
  _ -> error $ "buildStatement: Unsupported opcode " ++ show opcode

pass :: Build () -> Build Statement
pass = (>> return Pass)

debugOpCodeWith :: OpCode -> Int -> String -> Statement
debugOpCodeWith op arg s = Expr . Name $ show op ++ " " ++ show arg ++ ": " ++ s

valueToExpr :: Value -> Expr
valueToExpr = \case
  VCode c -> Function c
  VNone -> None
  VBool b -> Bool b
  VInt i -> Int i
  VFloat d -> Float d
  VStr s -> Str s
  VBytes s -> Bytes s
  VTuple vs -> Collection Tuple $ map (Item . valueToExpr) vs

mergeCollections :: Expr -> Expr -> Expr
mergeCollections (Collection c is) (Collection c' is')
  | c == c'   = Collection c $ is ++ is'
  | otherwise = error
    $ "mergeCollections: Trying to merge a " ++ show c ++ " and a " ++ show c'
mergeCollections (Name n) (Collection c is)
  = mergeCollections (nameToSingleton c n) (Collection c is)
mergeCollections (Collection c is) (Name n)
  = mergeCollections (Collection c is) (nameToSingleton c n)
mergeCollections a b = error
  $ "mergeCollections: Cannot merge " ++ show a ++ " and " ++ show b

nameToSingleton :: Collection -> Name -> Expr
nameToSingleton Dict n = Collection Dict [MappingUnpack n]
nameToSingleton c    n = Collection c [ItemUnpack n]

fixAST :: AST -> AST
fixAST
  = recMapStatements id insertPass
  . recMapStatements id (filter (/= Finally []))
  . recMapStatements id (filter $ not . dirtyStatement)
  . recMapStatements id (filter (/= Pass))
  . recMapStatements (buildComprehension . buildLambda) id
  . recMapStatements id (map promoteStatement)

dirtyStatement :: Statement -> Bool
dirtyStatement (Assign _ (Name n) _) = n `elem` dirtyNames
dirtyStatement (Return (Name n)) = n `elem` dirtyNames
dirtyStatement _ = False

dirtyNames :: [Name]
dirtyNames = ["__module__", "__qualname__", "__classcell__", "__class__"]

insertPass :: AST -> AST
insertPass [] = [Pass]
insertPass bs = bs

buildComprehension :: Expr -> Expr
buildComprehension (Call (Function c) [Item e])
  | [For ns _ [Append c' x], Return _] <- buildAST c
  = Comprehension c' x ns e
  | [For ns _ [Expr (Yield x)], Return _] <- buildAST c
  = Comprehension Gen x ns e
buildComprehension e = e

buildLambda :: Expr -> Expr
buildLambda (Function c)
  | [Return e] <- buildAST c
  = Lambda c e
buildLambda e = e

promoteStatement :: Statement -> Statement
promoteStatement (Assign _ n (Function c))
  = Def n c $ buildAST c
promoteStatement (Assign _ n (Call BuildClass (Item (Function c) : _ : is)))
  = Class n c is . removeLastReturn $ buildAST c
promoteStatement (Assign _ n (Call m [Item (Function c)]))
  = Decorator m . Def n c $ buildAST c
promoteStatement s = s

removeLastReturn :: AST -> AST
removeLastReturn [] = []
removeLastReturn ss
  | Return _ <- last ss = init ss
  | otherwise           = ss
