module Language.Python.Decompile.Build.PreProcess where

import Data.Bits

import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

cleanInstructions :: [Instruction] -> [Instruction]
cleanInstructions (i1 : i2 : is)
  | opcode i1 == EXTENDED_ARG = cleanInstructions
    $ i2 { argument = argument i1 `shiftL` 8 + argument i2 } : is
cleanInstructions (i : is)
  | opcode i `elem` noOps
  = cleanInstructions is
  | Just op <- lookup (opcode i) equivalent
  = i { opcode = op } : cleanInstructions is
  | otherwise
  = i : cleanInstructions is
cleanInstructions [] = []

-- | Operations that doesn't affect the decompilation.
noOps :: [OpCode]
noOps =
  [ -- Invisibly modifies TOS.
    GET_ITER
  , GET_YIELD_FROM_ITER
    -- Block marker not needed for decompilation.
  , SETUP_LOOP
  ]

-- | Mappings for opcodes that can be replaced with another.
equivalent :: [(OpCode, OpCode)]
equivalent =
  [ -- Only equivalent due to simplification in LOAD_METHOD.
    (CALL_METHOD, CALL_FUNCTION)
    -- These instructions load from the same category.
  , (LOAD_CLOSURE, LOAD_DEREF)
  , (LOAD_GLOBAL, LOAD_NAME)
    -- Simply an execution optimization.
  , (INPLACE_ADD, BINARY_ADD)
  , (INPLACE_MULTIPLY, BINARY_MULTIPLY)
  , (INPLACE_SUBTRACT, BINARY_SUBTRACT)
  , (INPLACE_TRUE_DIVIDE, BINARY_TRUE_DIVIDE)
  ]
