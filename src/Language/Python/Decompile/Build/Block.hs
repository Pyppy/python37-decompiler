module Language.Python.Decompile.Build.Block where

import Language.Python.Decompile.Block
import Language.Python.Decompile.Build.PreProcess
import Language.Python.Decompile.Code
import Language.Python.Decompile.OpCode

buildBlocks :: [Instruction] -> [Block]
buildBlocks = buildMost . cleanInstructions

buildMost :: [Instruction] -> [Block]
buildMost [] = []
buildMost is = bs ++ buildMost is'
  where (bs, is') = buildOne is

buildOne :: [Instruction] -> ([Block], [Instruction])
buildOne [] = ([], [])
buildOne (i : is) = case opcode i of
  FOR_ITER -> ([BFor (row i) ns . buildMost $ init is3], is5)
    where (i1 : is1) = is
          (ns, is2) = case opcode i1 of
            STORE_FAST -> ([argument i1], is1)
            STORE_NAME -> ([argument i1], is1)
            UNPACK_SEQUENCE -> (map argument $ take n is1, drop n is1)
              where n = argument i1
            op ->  error $ "FOR_ITER: Unexpected following opcode " ++ show op
          (is3, is4) = span ((< absolute i) . row) is2
          is5 = case is4 of
            (i' : is') | opcode i' == POP_BLOCK -> is'
            is' -> is'
  SETUP_EXCEPT -> (BTry (row i) try : bs, is2)
    where (try, is1, _) = buildUntil [POP_BLOCK] is
          (bs , is2)    = buildExcepts $ drop 1 is1
  SETUP_WITH -> ([BWith (row i) name with, BFinally (row $ head is3) fin], is4)
    where (i1  : is1 )   = is
          (with, is2, _) = buildUntil [POP_BLOCK] is1
          (_   , is3, _) = buildUntil [WITH_CLEANUP_FINISH] is2
          (fin , is4, _) = buildUntil [END_FINALLY] is3
          name = if opcode i1 == POP_TOP then Nothing else Just $ argument i1
  _ -> ([BInst i], is)

absolute :: Instruction -> Int
absolute i = row i + 2 + argument i

buildUntil :: [OpCode] -> [Instruction] -> ([Block], [Instruction], OpCode)
buildUntil op [] = error $ "buildUntil: Could not find opcodes " ++ show op
buildUntil op is
  | [BInst i] <- bs
  , opcode i `elem` op = ([], is', opcode i)
  | otherwise          = (bs ++ bs', is'', op')
  where (bs , is' )      = buildOne is
        (bs', is'', op') = buildUntil op is'

buildExcepts :: [Instruction] -> ([Block], [Instruction])
buildExcepts is = case op of
  POP_EXCEPT -> (makeExcept bs : bs', is'')
    where (bs', is'') = buildExcepts $ drop 1 is'
  END_FINALLY -> ([BFinally (row $ head is) bs], is')
  _ -> error "buildExcepts: Unreachable code."
  where (bs, is', op) = buildUntil [POP_EXCEPT, END_FINALLY] is

makeExcept :: [Block] -> Block
makeExcept (BInst i : bs)
  | opcode i == POP_TOP = BExcept (row i) Nothing              $ drop 2 bs
  | opcode i == DUP_TOP = BExcept (row i) (Just $ argument i') $ drop 5 bs'
  where (BInst i' : bs') = bs
makeExcept _ = error "makeExcept: Unrecognized except structure."
