module Language.Python.Decompile.Show where

import Data.List

-- | Pads a string with spaces on the left to a given length.
padLeft :: Int -> String -> String
padLeft m s = replicate (m - length s) ' ' ++ s

-- | Pads a string with spaces on the right to a given length.
padRight :: Int -> String -> String
padRight m s = s ++ replicate (m - length s) ' '

-- | Indents a string by four spaces for each indentation level.
indent :: Int -> String -> String
indent n s = concat (replicate n "    ") ++ s

-- | Maps the function on each element, then inserts the string between each
-- element, and finally concatenates the list.
interMap :: String -> (a -> String) -> [a] -> String
interMap s f = intercalate s . map f

-- | Formats a correct python tuple containing the results of mapping the given
-- function on each element.
properTuple :: (a -> String) -> [a] -> String
properTuple f [a] = "(" ++ f a ++ ",)"
properTuple f as  = "(" ++ interMap ", " f as ++ ")"
