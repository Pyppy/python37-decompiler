module Language.Python.Decompile.AST where

import Language.Python.Decompile.Code

type AST = [Statement]

-- | Represents a simple python statement.
data Statement
  -- | The no-op statement.
  = Pass
  -- | An expression as a statement.
  | Expr Expr
  -- | A return.
  | Return Expr
  -- | Exception raising.
  | Raise Expr
  -- | Look break.
  | Break
  -- | Deletion.
  | Del Expr
  -- | Variable assignment.
  | Assign Bool Expr Expr
  -- | An import statement.
  | Import Name [(Name, Name)]
  -- | Collection append that is used for comprehensions.
  | Append Collection Expr
  | Def Expr Code [Statement]
  | Decorator Expr Statement
  | Class Expr Code [Item] [Statement]
  | If Expr [Statement]
  | Else [Statement]
  | For [Name] Expr [Statement]
  | Try [Statement]
  | Except (Maybe Name) [Statement]
  | Finally [Statement]
  | With Expr (Maybe Name) [Statement]
  deriving (Show, Eq)

-- | Applies 'recMapExpr' on each expression in a statement and the second
-- function on each block of statements.
recMapStatements :: (Expr -> Expr) -> ([Statement] -> [Statement])
                 -> [Statement] -> [Statement]
recMapStatements f g = map rec' . g
  where rec' = \case
          Pass            -> Pass
          Expr e          -> Expr $ recMapExpr f e
          Return e        -> Return $ recMapExpr f e
          Raise e         -> Raise $ recMapExpr f e
          Break           -> Break
          Del e           -> Del $ recMapExpr f e
          Assign b e e'   -> Assign b (recMapExpr f e) (recMapExpr f e')
          Import n nns    -> Import n nns
          Append c e      -> Append c $ recMapExpr f e
          Def e c ss      -> Def (recMapExpr f e) c $ recMapStatements f g ss
          Decorator e s   -> Decorator (recMapExpr f e) $ rec' s
          Class e c is ss -> Class (recMapExpr f e) c (map (recMapItem f) is)
                             $ recMapStatements f g ss
          If e ss         -> If (recMapExpr f e) $ recMapStatements f g ss
          Else ss         -> Else $ recMapStatements f g ss
          For ns e ss     -> For ns (recMapExpr f e) $ recMapStatements f g ss
          Try ss          -> Try $ recMapStatements f g ss
          Except mn ss    -> Except mn $ recMapStatements f g ss
          Finally ss      -> Finally $ recMapStatements f g ss
          With e mn ss    -> With (recMapExpr f e) mn $ recMapStatements f g ss

-- | Represents the possible items that appears in python collections and calls.
data Item
  -- | A simple argument.
  = Item Expr
  -- | Unpacking of a collection of arguments.
  | ItemUnpack Name
  -- | A simple mapping.
  | Mapping Expr Expr
  -- | Unpacking of a collection of mappings.
  | MappingUnpack Name
  deriving (Show, Eq)

-- | Applies 'recMapExpr' on items containing expressions.
recMapItem :: (Expr -> Expr) -> Item -> Item
recMapItem f = \case
  Item e       -> Item $ recMapExpr f e
  Mapping e e' -> Mapping (recMapExpr f e) (recMapExpr f e')
  i            -> i

-- | The types of collections in python.
-- Gen is used to represent a generator and is included for simplicity.
data Collection = Tuple | List | Set | Dict | Gen deriving (Show, Eq)

-- | Represents a python expression.
data Expr
  -- * Values
  -- | Global or variable name.
  = Name Name
  -- | The None value.
  | None
  -- | Boolean values.
  | Bool Bool
  -- | Integer values.
  | Int Int
  -- | Floating point values.
  | Float Double
  -- | String values.
  | Str String
  -- | Byte string values.
  | Bytes String
  -- | Generalized expression for tuples, lists, sets and maps.
  | Collection Collection [Item]
  -- | Function definition as a value.
  | Function Code
  -- | Lambda expression.
  | Lambda Code Expr
  -- | List, set and dict comprehension.
  | Comprehension Collection Expr [Name] Expr
  -- * Built-ins
  -- | Build class function.
  | BuildClass
  -- * Operations
  -- | Negation.
  | Not Expr
  -- | Binary operation.
  | Binary String Expr Expr
  -- | Function call.
  -- * Other
  | Call Expr [Item]
  -- | Accessing the named attribute from an object.
  | Attr Expr Name
  -- | Indexing.
  | Index Expr Expr
  -- | Yield a value.
  | Yield Expr
  -- | Yield a value from a generator.
  | YieldFrom Expr
  deriving (Show, Eq)

-- | Recursively applies a function on each expression in top down order.
recMapExpr :: (Expr -> Expr) -> Expr -> Expr
recMapExpr f = rec' . f
  where rec' = \case
          Name n -> Name n
          None -> None
          Bool b -> Bool b
          Int i -> Int i
          Float d -> Float d
          Str s -> Str s
          Bytes s -> Bytes s
          Collection c is -> Collection c $ map (recMapItem f) is
          Function c -> Function c
          Lambda c e -> Lambda c $ recMapExpr f e
          Comprehension c e ns e'
            -> Comprehension c (recMapExpr f e) ns (recMapExpr f e')
          BuildClass -> BuildClass
          Not e -> Not $ recMapExpr f e
          Binary s e e' -> Binary s (recMapExpr f e) (recMapExpr f e')
          Call e is -> Call (recMapExpr f e) $ map (recMapItem f) is
          Attr e n -> Attr (recMapExpr f e) n
          Index e e' -> Index (recMapExpr f e) (recMapExpr f e')
          Yield e -> Yield $ recMapExpr f e
          YieldFrom e -> YieldFrom $ recMapExpr f e
