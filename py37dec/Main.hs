import System.Environment

import Language.Python.Decompile.CallPython
import Language.Python.Decompile.Build.AST
import Language.Python.Decompile.Show.AST

-- | The program takes the path to a pyc file as an argument,
-- decompiles it, and prints the result to standard out.
main :: IO ()
main = do
  args <- getArgs
  let path = atDefault 0 args id $ error "py37dec: No pyc file specified."
      py   = atDefault 1 args Just Nothing
  getCodeFromPython py path >>= putStrLn . showAST 0 . buildModule

atDefault :: Int -> [a] -> (a -> b) -> b -> b
atDefault _ []       _ b = b
atDefault 0 (a : _ ) f _ = f a
atDefault n (_ : as) f b = atDefault (n - 1) as f b
